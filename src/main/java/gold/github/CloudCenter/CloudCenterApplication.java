package gold.github.CloudCenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudCenterApplication.class, args);
	}

}
